﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerPrefs = PreviewLabs.PlayerPrefs; //PlayerPrefs最適化のためのUsing

public class MyGameDataStorageScript : MonoBehaviour
{

    private static bool created = false;


    public string MusicTitle;
    public string ArtistName;
    public string Genre;
    public float BPM;
    public int Perfect = 0;
    public int Great = 0;
    public int Good = 0;
    public int Bad = 0;
    public int Miss = 0;
    public int Score = 0;


    void Awake()
    {
        /*シーンを跨いでも変数の値の保持を実現するために、
        シーン切り替え時でも指定のオブジェクトを破棄されないようにする*/
        if (!created)
        {
            // シーンを切り替えても指定のオブジェクトを破棄せずに残す
            DontDestroyOnLoad(this.gameObject);
            // 生成した
            created = true;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnGUI()
    {
        GUI.Label(new Rect(10, 20, 120, 20), "SCORE : " + Score);
    }

    /* PlayerPrefs.Flush() を使用して全ての値を保存する。
     * これはデータを大量に書き込みすときか、アプリケーション終了時に行う(メインとなるゲーム クラスの中で) :*/
    //最適化として機能するために、Hashtableを保持して Flush() が呼ばれたときのみファイル書き込みするようにした。
    public void OnApplicationQuit()
    {
        PlayerPrefs.Flush();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicSequenceLoader : MonoBehaviour {

    private float BPM;
    private float MeasureTime;
    GameObject[] SqLine = new GameObject[3]; //SequenceLineそのもの
    MusicSequenceLine[] Script = new MusicSequenceLine[3]; //SequenceLineに付属させたScriptが入る変数

    // Use this for initialization
    void Start () {
        int i = 0;

        /*Scene上のSequenceLineの取得*/
        SqLine[0] = GameObject.Find("MusicSequenceLine");
        SqLine[1] = GameObject.Find("MusicSequenceLine (1)");
        SqLine[2] = GameObject.Find("MusicSequenceLine (2)");

        foreach(GameObject obj in SqLine){
            //MusicSequenceLine(GameObject)の中にあるMusicSequenceLine(Script)を取得
            Script[i] = obj.GetComponent<MusicSequenceLine>();
            i++;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //MusicSequenceLoaderから呼ばれる関数s
    public void MakeNote(int Measure, int Line, string NoteCode){
        int Num, StringNum = 0;
        float NoteTime;

        NoteTime = (Measure * MeasureTime)//該当小節の頭までにかかる時間を出す
                    + //小節の頭までの時間+小節内お時間オフセット=実際に譜面が配置される時間
                    StringNum *//文字数分だけ時間をかける 
                    (MeasureTime / NoteCode.Length);//文字の長さ(音符の分割数)で１の時間を破る

        //文字列を1文字ずつ分割して確認する
        foreach(char c in NoteCode){
            //char型からstring型への変換 & 確認した１文字が数字であれば処理
            if(int.TryParse(c.ToString(), out Num)){
                switch (Num)
                {
                    case 0:
                        Debug.Log("0");
                        break;
                    case 1:
                        Debug.Log("1");
                        Script[Line].AddNote(NoteTime, 0);
                        break;
                    case 2:
                        Debug.Log("2");
                        break;
                    default:
                        break;
                }
            }
            //数字でなければログを出す
            else{
                Debug.Log(c + " is not number.");
            }

            StringNum++;
        }

    }
}

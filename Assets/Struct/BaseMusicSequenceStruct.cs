﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMusicSequenceStruct : MonoBehaviour {
    
    [System.Serializable] //inspectorへ表示
    public struct MusicSequence
    {
        public string MusicTitle;
        public string ArtistName;
        public string Genre;
        public float BPM;
        public int MusicScore;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

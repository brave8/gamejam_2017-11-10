﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicSequenceLine : MonoBehaviour {

    //private Vector3 SqLinePos;
    private float BaseVelocity = 20;
    public bool IsPlaying = true;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if(IsPlaying){
            this.transform.localPosition -= new Vector3( 0, 0, BaseVelocity * Time.deltaTime);
        }
	}

    public void AddNote(float NoteTime, int Type){
        //プレハブを取得
        GameObject Prefab = (GameObject)Resources.Load("Note");
        GameObject Obj;

        /*Noteプレハブを生成して子供にする*/
        //プレハブからインスタンスを生成
        Obj = (GameObject)Instantiate(Prefab, transform.position, Quaternion.identity);
        // 作成したオブジェクトを子として登録
        Obj.transform.parent = transform;

        /*基本秒速と掛け算して配置場所を決め、譜面を配置*/
        Obj.transform.localPosition = new Vector3( 0, 0, NoteTime * BaseVelocity);
            
    }
}

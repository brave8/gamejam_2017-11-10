﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSPrevewScript : MonoBehaviour {

    public int MaxFrameRate = 60;
    int frameCount;
    float prevTime;

    private void Awake()
    {
        Application.targetFrameRate = MaxFrameRate;
    }

    // Use this for initialization
    void Start () {
        frameCount = 0;
        prevTime = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
        ++frameCount;
        float time = Time.realtimeSinceStartup - prevTime;

        if (time >= 0.5f){
            Debug.LogFormat("{0}fps", frameCount / time);

            frameCount = 0;
            prevTime = Time.realtimeSinceStartup;
        }
	}
}

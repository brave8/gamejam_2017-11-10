﻿using UnityEngine;
using System.Collections;

public class MyDebugTest : MonoBehaviour
{

    public Color color = Color.red;

    private void Awake()
    {
        MyDebug.Init();
    }

    private void OnGUI()
    {
        if (GUI.Button(new Rect(100, 0, 100, 100), "PUSH_LOG"))
        {
            MyDebug.PushLog("押されたで" + Time.time);
        }

        MyDebug.RenderLog(new Rect(0, 0, 100, 20), this.color);
    }

}